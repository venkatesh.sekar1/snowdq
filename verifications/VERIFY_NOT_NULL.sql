
/*
  Sample execution :
  call VERIFY_NOT_NULL(
    'SNOWFLAKE_SAMPLE_DATA','TPCDS_SF100TCL','CATALOG_RETURNS',
    PARSE_JSON('{
            "COL": "CR_WAREHOUSE_SK"
    }')
 );
*/
CREATE OR REPLACE PROCEDURE VERIFY_NOT_NULL (
  	PARAM_CATALOG VARCHAR, PARAM_SCHEMA VARCHAR, PARAM_TBL VARCHAR, 
  	PARAM_FN_CONFIG VARIANT)
RETURNS VARIANT NOT NULL
LANGUAGE JAVASCRIPT
COMMENT = '
    Validate if the column values are not null.
    Parameter : 
        {
            ''COL'': ''CR_WAREHOUSE_SK''
        }
'
AS
$$
    var failure_err_msg = [];
    var return_result_as_json = {};
    var sucess_count = 0;
    var failure_count = 0;
    
    const qry = `
    SELECT count(0) AS RECORD_COUNT_WITH_NULL,
        RECORD_COUNT_WITH_NULL > 0 AS VALIDATION_FAILED
    FROM "${PARAM_CATALOG}"."${PARAM_SCHEMA}"."${PARAM_TBL}"
    WHERE "${PARAM_FN_CONFIG["COL"]}" IS NULL
    ;`
    
    validation_failed = true;
    qry_result = {};
    try {
      var rs = snowflake.execute({ sqlText: qry });
      while (rs.next()) {
        qry_result['RECORD_COUNT_WITH_NULL'] = rs.getColumnValue('RECORD_COUNT_WITH_NULL');
        qry_result['VALIDATION_FAILED'] = rs.getColumnValue('VALIDATION_FAILED');
        validation_failed = qry_result['VALIDATION_FAILED'];
      }
      sucess_count = sucess_count + 1;
    } catch (err) {
        failure_count = failure_count + 1;
        failure_err_msg.push(` {
        sqlstatement : ‘${qry}’,
          error_code : ‘${err.code}’,
          error_state : ‘${err.state}’,
          error_message : ‘${err.message}’,
          stack_trace : ‘${err.stackTraceTxt}’
          } `);
    }
   
    return_result_as_json['VALIDATION_RESULT'] = qry_result;
    return_result_as_json['VALIDATION_FAILED'] = validation_failed;
    return_result_as_json['Success'] = sucess_count;
    return_result_as_json['Failures'] = failure_count;
    return_result_as_json['Failure_error'] = failure_err_msg;
    return return_result_as_json;
$$; 
