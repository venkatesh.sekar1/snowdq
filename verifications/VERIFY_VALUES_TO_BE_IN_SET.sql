
/*
 Sample execution :
 call VERIFY_VALUES_TO_BE_IN_SET (
    'SNOWFLAKE_SAMPLE_DATA','TPCDS_SF100TCL','STORE',
    PARSE_JSON('{
            "COL": "S_STATE",
            "VALUES": ["AL", "GA"]
    }')
  );
*/

CREATE OR REPLACE PROCEDURE VERIFY_VALUES_TO_BE_IN_SET (
	PARAM_CATALOG VARCHAR, PARAM_SCHEMA VARCHAR, PARAM_TBL VARCHAR, 
	PARAM_FN_CONFIG VARIANT)
RETURNS VARIANT NOT NULL
LANGUAGE JAVASCRIPT
COMMENT = '
    Validate if the values present in the column are part of the list.
    Parameter : 
        {
            ''COL'': ''STATE'',
            ''VALUES'': [''ON'', ''QC'', ''AB'']
        }
'
AS
$$
    var failure_err_msg = [];
    var return_result_as_json = {};
    var sucess_count = 0;
    var failure_count = 0;
    
    const eval_col = PARAM_FN_CONFIG["COL"];
    const allowed_vals = '\'' + PARAM_FN_CONFIG["VALUES"].join('\',\'') + '\'';
    const qry = `
    SELECT LISTAGG(DISTINCT "${PARAM_FN_CONFIG["COL"]}", ',') NOTALLOWED_VALS, 
        ARRAY_SIZE( SPLIT(NOTALLOWED_VALS, ',') ) NOTALLOWED_VAL_COUNT,
        NOTALLOWED_VAL_COUNT > 0 VALIDATION_FAILED
    FROM "${PARAM_CATALOG}"."${PARAM_SCHEMA}"."${PARAM_TBL}"
    WHERE "${PARAM_FN_CONFIG["COL"]}" NOT IN (${allowed_vals})
    ;`
    
    validation_failed = true;
    qry_result = {};
    try {
      var rs = snowflake.execute({ sqlText: qry });
      while (rs.next()) {
        qry_result['NOTALLOWED_VALS'] = rs.getColumnValue(1);
        qry_result['NOTALLOWED_VAL_COUNT'] = rs.getColumnValue(2);
        qry_result['VALIDATION_FAILED'] = rs.getColumnValue(3);
        validation_failed = qry_result['VALIDATION_FAILED'];
      }
      sucess_count = sucess_count + 1;
    } catch (err) {
        failure_count = failure_count + 1;
        failure_err_msg.push(` {
        sqlstatement : ‘${qry}’,
          error_code : ‘${err.code}’,
          error_state : ‘${err.state}’,
          error_message : ‘${err.message}’,
          stack_trace : ‘${err.stackTraceTxt}’
          } `);
    }
   
    return_result_as_json['VALIDATION_RESULT'] = qry_result;
    return_result_as_json['VALIDATION_FAILED'] = validation_failed;
    return_result_as_json['Success'] = sucess_count;
    return_result_as_json['Failures'] = failure_count;
    return_result_as_json['Failure_error'] = failure_err_msg;
    return return_result_as_json;
$$;


