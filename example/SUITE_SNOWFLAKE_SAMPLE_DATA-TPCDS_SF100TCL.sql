--
-- Example configuration for performing verification on snowflake sample dataset.
-- Note these declarations are done for example and validation will fail, purposefully
-- configured to fail, but enough to demonstrate the concept.
--

insert into DQ_EXPECTATION_CONFIG  select
    'SNOWFLAKE_SAMPLE_DATA/TPCDS_SF100TCL',
    'STORE_STATE_ALLOWED_VALUES',
    'SNOWFLAKE_SAMPLE_DATA','TPCDS_SF100TCL','STORE', 1,
    'VERIFY_VALUES_TO_BE_IN_SET',
    PARSE_JSON('{
            "COL": "S_STATE",
            "VALUES": ["AL", "GA"]
    }'),
    '#BASIC', 'Test if states have values not in the list',
    true,CURRENT_TIMESTAMP()
;

--
-- This verification could take some time to execute, due to the size of the sample     data.
--
insert into DQ_EXPECTATION_CONFIG  select
    'SNOWFLAKE_SAMPLE_DATA/TPCDS_SF100TCL',
    'CATALOG_RETURNS_WAREHOUSE_SHOULDNOT_BE_EMPTY',
    'SNOWFLAKE_SAMPLE_DATA','TPCDS_SF100TCL','CATALOG_RETURNS',2,
    'VERIFY_NOT_NULL',
    PARSE_JSON('{
            "COL": "CR_WAREHOUSE_SK"
    }'),
    '#BASIC', 'warehouse should not be empty',
    true,CURRENT_TIMESTAMP()
;