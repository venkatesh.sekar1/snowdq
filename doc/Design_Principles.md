
While the adoption will take some initiative, timelines, discussions etc the need for performing data quality checks should not be postponed till some time in the future. In this regards, I am presenting a tactical alternative solution **SnowDQ**. 

To aid my fellow data engineers and Snowflaker's, **SnowDQ** is a simplistic tactical approach to performing data quality checks on the data hosted in Snowflake. The code is available in our GitLab and open sourced.

## Design Principles
Before we get into implementation details, here are the various aspects as to why this was developed and being shared out.

### Adoption Boundaries
Tools such as **SODA** are aimed and developed to be adopted for multiple platforms i.e., Spark, BigQuery, Snowflake, Synapse etc… Obviously the underlying code/implementation have to be written in a generic way. With the vision of being generic, the adoption boundary is enterprise wide.

**SnowDQ** has been developed specifically for Snowflake, hence meant to be used by developers, analysts and support teams managing the Snowflake environment. Hence keeping it simple.

### Operating environment and Language Boundaries
**SnowDQ**, utilizes what Snowflake has to offer as part of its solution. Snowflake scalable warehouse offers compute environment, so no need to maintain, host and administer a separate set of resources like Docker, VM etc…

The verification/expectation functions are native Store Procedure, Javascript offered by Snowflake. Hence teams do not have to learn anything new. Custom verification will also need to be implemented as Store Procedure.

Keeping **SnowDQ** native to Snowflake, you do not need to worry about getting in discussion with infrastructure, security, network, cloud system admin, procurement etc.. Hence adoption of this tactical solution, can be done as part of your current adoption/deliverables. 

### Data security and vulnerability
Snowflake javascript store-procedure cannot use any custom libraries and is not allowed to reach outside of Snowflake. Hence no data gets pushed outside of the compute.

The verification/expectation store-procedure can be declared to be runnable as "Owner" or "caller". Hence no need to re-design new security roles, for scenarios accessing secure dataset. 

Since store-procedure are bounded within the context of Snowflake, your adoption timeline is reduced in the context of getting security approvals.

### Invocation of expectation suite
**SnowDQ**, verifications can be executed by a single call ,for ex:

```sql
-- Invoke a whole expectations suite
call VERIFY_EXPECTATIONS('SNOWFLAKE_SAMPLE_DATA/TPCDS_SF100TCL', true, parse_json('{ "BATCH":456 }'));
-- Invoke a specific expectation
call VERIFY_NOT_NULL(
    'SNOWFLAKE_SAMPLE_DATA','TPCDS_SF100TCL','CATALOG_RETURNS',
    PARSE_JSON('{
            "COL": "CR_WAREHOUSE_SK"
    }')
);
```

This allows verification from different tool Matillion, DBT, custom sql script etc… Hence no new tool specific integrations, need to be developed.

### Validation result data store and accessibility
**SnowDQ**, verification store-procedure can optionally store the result in a table. The result are stored as a VARIANT (json) format.

![](./media/verification_result.png)

You can now build view on top of this table and have it served in BI Dashboards, web apps etc…

