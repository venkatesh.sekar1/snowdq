# Performing Data Quality checks in Snowflake - Part 1
## Dated : Apr-2021
----

Data quality and data monitoring is recent aspect of data engineering practices that I had stumbled upon these days. Regardless of your journey for a data catalog/governance; I truly feel that having a "Continuous data quality" checks and "Continuous data monitoring" is a definite need. 

### What is Data quality/monitoring, you ask ?
Well I guess, it is very well discussed here in this "Data Engineering " podcast, episode #178. To start of I would highly recommend listening to this episodes as the Founders of Soda clearly articulate and explain the what's, why's and how's :
[Data Quality Management For The Whole Team With Soda Data](https://www.dataengineeringpodcast.com/soda-data-quality-management-episode-178/)

To me, while we perform various unit test and other scenario to the code and ensure it can handle the business logic. It however fails in the real production environment, as the underlying data changes, things we really did not account for.

For example, a feed from external source could change the datatype or structure and this will definitely break somewhere in the data processing pipeline. Being aware of such change and alerting interested parties would reduce a lot of headache.

Performing "Continous" data quality/monitoring, to me, is like all those dials/controls in the dashboard of a train/airplane cockpit. They alert you if something is off (meaning data here) and point you to problem (ex: probelem records).

### What are the choices ?
Hopefully you are convinced and want to know, the various choices. The good news is that we have a wide variety of players in this field. 
- [Great Expectations](https://greatexpectations.io/) : (my favourite), open source, python based and adopted widely.
- [SODA](https://www.soda.io) : part open source and based of the above podcast, the founders seems to have the right formula and approaches. python based.
- [DataFold](https://www.datafold.com)
- [Anomalo](https://www.anomalo.com)
...

### Ok, so what are we discussing next ?
Well each of the vendor/product above are well developed and adopted, I however feel that there are some missing aspects that is not accounted for. I want to highlight some of these and let you be the best judge of deciding your journey into "Continuous" data quality/monitoring.

These aspects are based of my various engagement as a data migration consultant, covering various sectors and team size etc…

#### Not all teams are the same
I have come across teams, who are just starting on their cloud adoption/data migration journey. They are still learning the ropes and knots of developing, promoting and executing data pipelines. To such basic SQL data engineers, python is a stretch bit much. Hence knowledge of python, virtual env, developing custom expectations etc.. is a new thing.

While I do try to educate on onboarding a SQL/python data engineers as a necessity, it does not happen frequently. Hence some clients shy away from python based adoption, at-least initially.

If you want to adopt tools like Great Expectations, do consider onboarding 1 or 2 python based resources into the data engineering team. Do consider the training involved for existing resources. Do find a way, of reaching your sister departments to aid in adoption.

#### Not all clients are the same
I like working with smaller/medium sized companies like startups etc.. They are agile, nimble and open to adoption of right tools to reach the goal. However in larger organization, the adoption of tools is a long stretch. I still have enterprise clients, not adopting "dbt".

Part of the reason ranges from Enterprise standards, Enterprise architecture approvals, Security assessments, need for commercial support and also procurement lifecycle. 

If you want to adopt tools, do consider the average timeline of getting the product installed in a dev workstation and also the process involved. Do look to see, if vendors like Hashmap or product vendor like superconductive(GreatExpectations), SODA offer advices on adoptions and guide you in the process.

#### Not all operating environments are the same
The tool like "Great Expectations", "SODAsql" needs a python based environment to run. They could be wrapped into a docker container and deployed in a VM, k8s cluster etc.. However based on the vertical sector, ex: Insurance, Finance etc.. the choice of docker/vm image would be restricted. Part of these could be due to industry compliance like HIPAA etc..

The need to host the tool and compute is minimal, but do consider the process involved in getting the right type of compute. Do consider the approvals, meetings etc with various department heads to get that infrastructure in place for the adoption.

#### Not all data security/compliance are the same 
Based of the data being verified, data security policies can affect on the adoption. For example, if a certain expectation/validation query involves checking PII records, we need to understand whats involved in the process of verification. 
 - Does the data gets extracted out of the data store (ex: Snowflake) and sent over the wire to hosting/compute environment where the tool reside ? 
 - What is the user role / grants needed for this verification to be made ?
 - Where is the temporary storage, if any, for storing the data intermediately as the verification is performed ? 
 - How to ensure that the data is cleansed, once verification is done ?

The above are just a few pointers. If you are considering the adoption ensure the above points are addressed. Even if your data is not PII / other compliance specific, capturing these and documenting these will prevent future headaches.

#### Not all support team are the same
Support team, which is usually a separate department that manages the application in production, would have all the necessary skillsets in proper execution of the data quality/monitoring tool. They however would have rich knowledge on data issues, as they would have encountered many and addressed them.

Involving the team on developing expectations and metrics will aid in successful adoption. However, do consider the timelines and process involved in training, mentoring etc with the support team.

### Thoughts
I hope that I did not scare you to not adopt data quality/monitoring. All, I did was raise awareness of some aspects involved in the adoption journey. 
While adopting the open-source/vendor product would be a good choice. Until such time, I wanted to share an alternative tactical approach to aid in the "continuous" data quality/monitoring. 