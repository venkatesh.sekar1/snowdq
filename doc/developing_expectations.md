
# Developing verification / expectation functions

Verification/expectations are simply Javascript based Stored Procedures, which validates specific conditions on specific table or set of tables.

## Procedure signature/contract

The verification signature needs to adopt the following signature/contract

### Parameters
It shoud have the following parameters:
- PARAM_CATALOG : The database where the table that needs to be verified resides.
- PARAM_SCHEMA : The schema where the table that needs to be verified resides.
- PARAM_TBL : The subject table upon which verification is done.
- PARAM_FN_CONFIG : A variant based config that is specific to the expectation implementation.

### Return type
The function should return a VARIANT (json) formatted result only.
The json should have at the least the following keys:
 - VALIDATION_FAILED : True/False indicating the status of verification.
 - VALIDATION_RESULT : An optional nested json element, which contain further details on failures.

## Example walk thru - 1

Lets walk thru an example implementation [VERIFY_NOT_NULL](../verifications/VERIFY_NOT_NULL.sql).

### Invocation
```sql
call VERIFY_NOT_NULL(
    'SNOWFLAKE_SAMPLE_DATA','TPCDS_SF100TCL','CATALOG_RETURNS',
    PARSE_JSON('{
            "COL": "CR_WAREHOUSE_SK"
    }')
);
```
Lets say our expectation is that the there should not be any records where the warehouse is null on the table 'SNOWFLAKE_SAMPLE_DATA.TPCDS_SF100TCL.CATALOG_RETURNS'.

In the above we are validating if there are any records which violates this rule, and if there are return the count of records.

#### PARAM_FN_CONFIG
 For this verification function, it requires a COL parameter which is used to validate the nullability check on.

#### Result
```json
{
  "Failure_error": [],
  "Failures": 0,
  "Success": 1,
  "VALIDATION_FAILED": true,
  "VALIDATION_RESULT": {
    "RECORD_COUNT_WITH_NULL": 288601918,
    "VALIDATION_FAILED": true
  }
}
```

The above is the sample result returned from the invocation, it failed indicating the failure and also the count of records.

## Example walk thru - 2

Lets walk thru an example implementation [VERIFY_VALUES_TO_BE_IN_SET](../verifications/VERIFY_VALUES_TO_BE_IN_SET.sql).

### Invocation
```sql
call VERIFY_VALUES_TO_BE_IN_SET(
    'SNOWFLAKE_SAMPLE_DATA','TPCDS_SF100TCL','STORE',
    PARSE_JSON('{
            "COL": "S_STATE",
            "VALUES": ["AL", "GA"]
    }')
);
```

In this, we can see the parameter is not the same as in the previous example, it is different. Thus we can vary the parameter based on the verification/expectation needs.

#### Result
Again the validation was purposely configured to fail. Here is the response
```json
{
  "Failure_error": [],
  "Failures": 0,
  "Success": 1,
  "VALIDATION_FAILED": true,
  "VALIDATION_RESULT": {
    "NOTALLOWED_VALS": "NE,KY,KS,MT,CA,VA,WV,AZ,MO,IN,MS,MA,OR,SC,IA,NC,WI,TX,CO,MN,ND,NM,NH,UT,MI,OH,LA,WA,FL,RI,AK,ME,TN,NJ,PA,MD,NY,ID,VT,SD,IL,DC,CT,OK,AR",
    "NOTALLOWED_VAL_COUNT": 45,
    "VALIDATION_FAILED": true
  }
}
```
