
--
-- Example Usage (1) :
-- call VERIFY_EXPECTATIONS('SNOWFLAKE_SAMPLE_DATA/TPCDS_SF100TCL', false, '{}');
-- or
-- call VERIFY_EXPECTATIONS('SNOWFLAKE_SAMPLE_DATA/TPCDS_SF100TCL', false, NULL);
--

CREATE OR REPLACE PROCEDURE VERIFY_EXPECTATIONS(
  PARAM_SUITE_ID VARCHAR,                       -- The verification suite to run.
  PARAM_STORE_RESULT BOOLEAN,     -- Should the result be stored in the DQ_VERIFICATION_RUNS table.
  PARAM_METADATA VARIANT -- JSON formatted string, which offers metadata to be added to result, for traceabilibity needs. If provided, should be a valid JSON
)
RETURNS VARIANT NOT NULL
LANGUAGE JAVASCRIPT
COMMENT = '
    Executes the list of all expectations defined in DQ_EXPECTATION_CONFIG for
    the parameter suite.
'
AS
$$
    function run_expecation_fun(p_fn, p_catalog, p_schema, p_table, p_fnconfig) {
        var res = {}
        const fnqry = `
                call ${p_fn}('${p_catalog}', '${p_schema}', '${p_table}', parse_json('${p_fnconfig}') );
            `;
        try {
            var rs = snowflake.execute({ sqlText: fnqry });
            while (rs.next()) {
                res['VALIDATION_RESULT'] = rs.getColumnValue(1);
            }
        } catch (err) {
            failure_err_msg.push(` {
            sqlstatement : ‘${fnqry}’,
              error_code : ‘${err.code}’,
              error_state : ‘${err.state}’,
              error_message : ‘${err.message}’,
              stack_trace : ‘${err.stackTraceTxt}’
              } `);
        }
        res['Failure_error'] = failure_err_msg;
        return res;
    };
    
    function record_verification(p_status) {
        var res = {}
        try {
            const status_str = JSON.stringify(p_status)
            const inst_stmt = `
                insert into DQ_VERIFICATION_RUNS values( '${p_status}' );
            `;
            var rs = snowflake.execute({ 
                        sqlText: 'insert into DQ_VERIFICATION_RUNS select PARSE_JSON(:1) ;',
                        binds: [status_str]
                    });
            while (rs.next()) {
                res['STORE_RESULT'] = rs.getColumnValue(1);
            }
        } catch (err) {
            failure_err_msg.push(` {
              sqlstatement : ‘DQ_VERIFICATION_RUNS insert’,
              error_code : ‘${err.code}’,
              error_state : ‘${err.state}’,
              error_message : ‘${err.message}’,
              stack_trace : ‘${err.stackTraceTxt}’
              } `);
        }
        res['Failure_error'] = failure_err_msg;
        return res;
    };
    
    var failure_err_msg = [];
    var sucess_count = 0;
    var failure_count = 0;
    
    const qry = `
    SELECT 
        TO_CHAR(current_timestamp(), 'YYMMDDHH24MISS') RUN_ID,
        SUITE_ID,
        EXPECTATION, SUBJECT_CATALOG, SUBJECT_SCHEMA, SUBJECT_TABLE,
        EXPECTATION_FN, FN_PARAM,
        CURRENT_TIMESTAMP() AS EXEC_TIME
    FROM DQ_EXPECTATION_CONFIG
    WHERE SUITE_ID = '${PARAM_SUITE_ID}'
        AND ENABLED = TRUE
    ORDER BY POS
    ;`
    
    expectations = [];
    try {
      var rs = snowflake.execute({ sqlText: qry });
      while (rs.next()) {
        rec = {};
        rec['METADATA'] = PARAM_METADATA;
        rec['RUN_ID'] = rs.getColumnValue('RUN_ID');
        rec['SUITE_ID'] = rs.getColumnValue('SUITE_ID');
        rec['EXPECTATION'] = rs.getColumnValue('EXPECTATION');
        rec['EXEC_TIME'] = rs.getColumnValue('EXEC_TIME');
        rec['SUBJECT_CATALOG'] = rs.getColumnValue('SUBJECT_CATALOG');
        rec['SUBJECT_SCHEMA'] = rs.getColumnValue('SUBJECT_SCHEMA');
        rec['SUBJECT_TABLE'] = rs.getColumnValue('SUBJECT_TABLE');
        rec['EXPECTATION_FN'] = rs.getColumnValue('EXPECTATION_FN');
        rec['FN_PARAM'] = rs.getColumnValueAsString('FN_PARAM');
        expectations.push(rec);
      }
      sucess_count = sucess_count + 1;
    } catch (err) {
        failure_count = failure_count + 1;
        failure_err_msg.push(` {
        sqlstatement : ‘${qry}’,
          error_code : ‘${err.code}’,
          error_state : ‘${err.state}’,
          error_message : ‘${err.message}’,
          stack_trace : ‘${err.stackTraceTxt}’
          } `);
    }
   
    verifications = expectations.map(rec => {
        rec['RESULT'] = run_expecation_fun(rec['EXPECTATION_FN'], 
            rec['SUBJECT_CATALOG'], rec['SUBJECT_SCHEMA'], rec['SUBJECT_TABLE'], 
            rec['FN_PARAM']);
        
        delete rec['SUBJECT_CATALOG'];
        delete rec['SUBJECT_SCHEMA'];
        delete rec['SUBJECT_TABLE'];
        delete rec['FN_PARAM'];
        
        if (PARAM_STORE_RESULT == true) {
            record_verification(rec);
        }
        
        return rec;
    });
    
    var return_result_as_json = {};
    return_result_as_json['verifications'] = verifications;
    return_result_as_json['Success'] = sucess_count;
    return_result_as_json['Failures'] = failure_count;
    return_result_as_json['Failure_error'] = failure_err_msg;
    return return_result_as_json;
$$;

