
CREATE OR REPLACE VIEW VERIFICATION_RUN_STATUS_VW 
COMMENT='A helper view that returns the status of the verifications'
AS
SELECT 
    verifications:RUN_ID::VARCHAR AS RUN_ID, 
    verifications:SUITE_ID::VARCHAR AS SUITE_ID,
    verifications:EXPECTATION::VARCHAR AS EXPECTATION,
    verifications:RESULT.VALIDATION_RESULT.VALIDATION_FAILED::BOOLEAN AS VALIDATION_FAILED
FROM DQ_VERIFICATION_RUNS
;

